import smtplib
import sys
from xml.dom import minidom


def stripTag (xmlElement):
	#xmlElement = xmlElement.toxml()
	position = xmlElement.find('>')
	tagName = xmlElement[1:position]
	openingTag = '<' + tagName + '>'
	closingTag = '</' + tagName + '>'
	tagData = xmlElement.replace(openingTag,'').replace(closingTag,'')
	return tagData

# configuration XML file must be in the same directory
xmldoc = minidom.parse('config.xml')

# configNode is the main node
configNode = xmldoc.firstChild
# Email variable xml parsing
emailNode = configNode.childNodes[3]
smtpNode = emailNode.childNodes[1]
smtpServer = stripTag(smtpNode.childNodes[3].toxml())
smtpPort = stripTag(smtpNode.childNodes[7].toxml())
smtpUsername = stripTag(smtpNode.childNodes[11].toxml())
smtpPassword = stripTag(smtpNode.childNodes[13].toxml())

try:
	fromAddress = sys.argv[1]
	toAddress = sys.argv[2]
	subject = sys.argv[3]
	body = sys.argv[4]
except:
	exit(print('Required arguments are not present'))	# Reports error and exits program





	
try:
	msg = ('From: %s\r\nTo: %s\r\nSubject: %s\r\n' 
			%(fromAddress,toAddress,subject))

	msg = msg + body

	server = smtplib.SMTP(smtpServer,smtpPort)
	server.login(smtpUsername,smtpPassword)
	server.sendmail(fromAddress, toAddress, msg)
	server.quit()
except:
	exit(print('There was an error sending the current email.'))	# Reports error and exits program
