import re
import os
import datetime
import subprocess
from xml.dom import minidom

rootDirectory = 'c:/autoshot/'
snapShotexe = 'c:/autoshot/snapshot.exe'


os.chdir(rootDirectory)

def stripTag (xmlElement):
	#xmlElement = xmlElement.toxml()
	position = xmlElement.find('>')
	tagName = xmlElement[1:position]
	openingTag = '<' + tagName + '>'
	closingTag = '</' + tagName + '>'
	tagData = xmlElement.replace(openingTag,'').replace(closingTag,'')
	return tagData

xmlConfig = rootDirectory + 'config.xml'
# configuration XML file must be in the same directory
xmldoc = minidom.parse(xmlConfig)

# configNode is the main node
configNode = xmldoc.firstChild
# backup0Node contains all the variables for the job
backup0Node = configNode.childNodes[1]
# sourceDrive Section with three useful subnodes
srcdrvNode = backup0Node.childNodes[1]
# destinationNode has 3 subnodes
destinationNode = backup0Node.childNodes[5]

snaDir = stripTag(destinationNode.childNodes[1].toxml())
subMonth = stripTag(destinationNode.childNodes[3].toxml())
hashDir = stripTag(destinationNode.childNodes[7].toxml())
logDir = stripTag(destinationNode.childNodes[11].toxml())

schedule0Node = backup0Node.childNodes[7]

# to get subnodes use schd0*day*Node.childNodes[1,3,5] 1=backup 3=type 5=defrag
schd0monNode = schedule0Node.childNodes[3]
schd0tuesNode = schedule0Node.childNodes[5]
schd0wedNode = schedule0Node.childNodes[7]
schd0thurNode = schedule0Node.childNodes[9]
schd0friNode = schedule0Node.childNodes[11]
schd0satNode = schedule0Node.childNodes[13]
schd0sunNode = schedule0Node.childNodes[15]

# Options xml parsing
optionsNode = schedule0Node.childNodes[17]
limitNode = stripTag(optionsNode.childNodes[1].toxml())
sizeNode = stripTag(optionsNode.childNodes[5].toxml())
recycleNode = stripTag(optionsNode.childNodes[9].toxml())
waitNode = stripTag(optionsNode.childNodes[13].toxml())
graphicalNode = stripTag(optionsNode.childNodes[17].toxml())
testNode = stripTag(optionsNode.childNodes[21].toxml())
passwordNode = stripTag(optionsNode.childNodes[25].toxml())
excludeNode = stripTag(optionsNode.childNodes[29].toxml())

# Email variable xml parsing
emailNode = configNode.childNodes[3]
recipiantsNode = emailNode.childNodes[7]
smtpNode = emailNode.childNodes[1]
smtpServer = stripTag(smtpNode.childNodes[3].toxml())
smtpPort = stripTag(smtpNode.childNodes[7].toxml())
smtpUsername = stripTag(smtpNode.childNodes[11].toxml())
smtpPassword = stripTag(smtpNode.childNodes[13].toxml())
fromAddress = stripTag(emailNode.childNodes[3].toxml())
firstRecipiant = stripTag(recipiantsNode.childNodes[3].toxml())


# Text message variable xml parsing
textmessageNode = emailNode.childNodes[9]
carrier = stripTag(textmessageNode.childNodes[1].toxml())
number = stripTag(textmessageNode.childNodes[3].toxml())
booleanText = stripTag(textmessageNode.childNodes[7].toxml())
textSender = stripTag(textmessageNode.childNodes[11].toxml())
booleanText = bool(booleanText.upper())

carrierList = rootDirectory + 'carriers.lst'

# Parsing the carriers.lst file for the format of the text email address
if os.path.exists(carrierList):
	carrierList = open(carrierList, 'r')
	carrierLines = carrierList.readlines()
	carrierList.close()
	lineCount = len(carrierLines)
else:
	fileFound = 0
	sys.exit(print('Carrier list input File Not Found'))

carrierLength = len(carrierLines)

for each in range(0,carrierLength):
	carrierTest = carrierLines[each].find(carrier)
	if carrierTest >= 0:
		addressFormat = carrierLines[each+1]
		addressFormat = addressFormat.replace('XXX-XXX-XXXX',number)


# Get computer name
compName = os.getenv('COMPUTERNAME')

# Message body for text message
textMessage = """There was an error with %s's backup""" %(compName)


# Count and format Drives to be backed up
var = 0			# Step counter to increment srcdrvNode xmlelement
drvList = []	# List for drive letter storage
gate = 1		# This is the while loop control variable that stops things once an error occurs, The gate is either open (1) or closed (0)

while gate == 1:
	try:
		element = srcdrvNode.childNodes[var].toxml()					# save the current childNode to element
		frmtdElement = stripTag(element)								# element is passed to stripTag to do just what you think and the data is saved to frmtdElement

		if re.match("^[A-Za-z]+$", frmtdElement):						# check that the data is an alpha character
			if len(frmtdElement) < 2:									# is there only one character?
				frmtdElement = frmtdElement.upper() 					# Everybody is uppercase!!!!!
				frmtdElement = frmtdElement+':'							# adds : to meet syntax requirements
				drvList.append(frmtdElement)							# if yes slap it at the end of the drvList
		var = var + 1													# increment var and go check the next childNode
	except:																# uh oh! things have gone askew
		gate = 0														# we have reached the end of our childNodes
		if len(drvList) == 0:											# if the length of drvList is 0 then there was no data collected
			print('XML element contained no drive letter information')	# print statement will be changed to a return




# current date
curDate = datetime.datetime.now()
curDate = str(curDate)
curMonth = curDate[5:7]
curMonth = int(curMonth)
curDate = curDate[0:10]

months = ['January','February','March','April','May','June','July','August','September','October','November','December']

curMonth = months[curMonth - 1]


# Day of the week (0 = monday 6 = sunday)
dayOweek = datetime.datetime.now()
dayOweek = dayOweek.weekday()

# decides what node to read for the correct day of the week and pulls the settings for that day
if dayOweek == 0:
	backupProceed = schd0monNode.childNodes[1].toxml()
	backType = schd0monNode.childNodes[3].toxml()
	defragProceed = schd0monNode.childNodes[5].toxml()
elif dayOweek == 1:
	backupProceed = schd0tuesNode.childNodes[1].toxml()
	backType = schd0tuesNode.childNodes[3].toxml()
	defragProceed = schd0tuesNode.childNodes[5].toxml()
elif dayOweek == 2:
	backupProceed = schd0wedNode.childNodes[1].toxml()
	backType = schd0wedNode.childNodes[3].toxml()
	defragProceed = schd0wedNode.childNodes[5].toxml()
elif dayOweek == 3:
	backupProceed = schd0thurNode.childNodes[1].toxml()
	backType = schd0thurNode.childNodes[3].toxml()
	defragProceed = schd0thurNode.childNodes[5].toxml()
elif dayOweek == 4:
	backupProceed = schd0friNode.childNodes[1].toxml()
	backType = schd0friNode.childNodes[3].toxml()
	defragProceed = schd0friNode.childNodes[5].toxml()
elif dayOweek == 5:
	backupProceed = schd0satNode.childNodes[1].toxml()
	backType = schd0satNode.childNodes[3].toxml()
	defragProceed = schd0satNode.childNodes[5].toxml()
elif dayOweek == 6:
	backupProceed = schd0sunNode.childNodes[1].toxml()
	backType = schd0sunNode.childNodes[3].toxml()
	defragProceed = schd0sunNode.childNodes[5].toxml()


backupProceed = stripTag(backupProceed)
backupProceed = backupProceed.upper()
bool(backupProceed)


backType = stripTag(backType)
defragProceed = stripTag(defragProceed)

subMonth = subMonth.upper()

try:
	os.makedirs(snaDir+'\\'+curMonth)#[, mode])
	commandString = []
	driveCount = 0
except:
	commandString = []
	driveCount = 0




file = open(logDir+'\\'+curDate+'_'+compName+'_backup.log','w')


# Build and execute backup command for each drive
for each in drvList:
	commandString.append(snapShotexe)
	commandString.append(drvList[driveCount])
	snaString = compName+'_'+drvList[driveCount][0]+'_'+curDate+'_'+backType+'.sna'
	if subMonth == 'TRUE':
		snaString = snaDir+'\\'+curMonth+'\\'+snaString
	elif subMonth == 'False':
		snaString = snaDir+'\\'+snaString

	commandString.append(snaString)

	if backType == 'full':
		hashString = '-O'+hashDir+'\\'+drvList[driveCount][0]+'-hash.hsh'
	elif backType == 'diff':
		hashString = '-h'+hashDir+'\\'+drvList[driveCount][0]+'-hash.hsh'	

	commandString.append(hashString)

	if limitNode != '':
		limitNode = '-L'+limitNode
		commandString.append(limitNode)

	if sizeNode != '':
		sizeNode = '-S'+sizeNode
		commandString.append(sizeNode)

	if recycleNode == 'yes':
		recycleNode = '-R'
		commandString.append(recycleNode)

	if waitNode == 'yes':
		waitNode = '-W'
		commandString.append(waitNode)

	if graphicalNode != '':
		graphicalNode = '-'+graphicalNode
		commandString.append(graphicalNode)

	if testNode == 'True':
		testNode = '-T'
		commandString.append(testNode)

	if passwordNode != '':
		passwordNode = '-PW='+passwordNode
		commandString.append(passwordNode)

	if excludeNode != '':
		excludeNode = '--exclude:'+excludeNode
		commandString.append(excludeNode)


	if len(commandString) == 3 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]

	elif len(commandString) == 4 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]

	elif len(commandString) == 5 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]

	elif len(commandString) == 6 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4],commandString[5]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]+' '+commandString[5]

	elif len(commandString) == 7 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4],commandString[5],commandString[6]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]+' '+commandString[5]+' '+commandString[6]

	elif len(commandString) == 8 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4],commandString[5],commandString[6],commandString[7]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]+' '+commandString[5]+' '+commandString[6]+' '+commandString[7]

	elif len(commandString) == 9 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4],commandString[5],commandString[6],commandString[7],commandString[8]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]+' '+commandString[5]+' '+commandString[6]+' '+commandString[7]+' '+commandString[8]

	elif len(commandString) == 10 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4],commandString[5],commandString[6],commandString[7],commandString[8],commandString[9]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]+' '+commandString[5]+' '+commandString[6]+' '+commandString[7]+' '+commandString[8]+' '+commandString[9]

	elif len(commandString) == 11 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4],commandString[5],commandString[6],commandString[7],commandString[8],commandString[9],commandString[10]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]+' '+commandString[5]+' '+commandString[6]+' '+commandString[7]+' '+commandString[8]+' '+commandString[9]+' '+commandString[10]

	elif len(commandString) == 12 and backupProceed == 'TRUE':
		output = subprocess.call([commandString[0],commandString[1],commandString[2],commandString[3],commandString[4],commandString[5],commandString[6],commandString[7],commandString[8],commandString[9],commandString[10],commandString[11]], stdout = file)
		command = commandString[0]+' '+commandString[1]+' '+commandString[2]+' '+commandString[3]+' '+commandString[4]+' '+commandString[5]+' '+commandString[6]+' '+commandString[7]+' '+commandString[8]+' '+commandString[9]+' '+commandString[10]+' '+commandString[11]

file.close()

try:
	file = open(logDir+'\\'+curDate+'_'+compName+'_backup.log','r')
	readFile = file.read()
	file.close()

	commandString = ' '.join(commandString)

	subject = """An Error occured with %s's backup""" %(compName)

	messageBody = """
	There was a general error with the backup for %s

	Here is what we know:
	==========================================================
	Error Code: %d

	Command Issued:
	%s

	Log File Content:
	""" %(compName,output,commandString)

	messageBody = messageBody + readFile

	if output == 0:
		subprocess.call(['python',rootDirectory + 'RepGen.py',logDir+'\\'+curDate+'_'+compName+'_backup.log'])

	else:
		#print(addressFormat)
		#print(fromAddress)
		#print(textMessage)
		try:
			print('There was an error with execution')
			subprocess.call(['python',rootDirectory + 'Emailer.py',fromAddress,firstRecipiant,subject,messageBody])
			#subprocess.call(['python',rootDiretory + 'Emailer.py',fromAddress,addressFormat,'Alert',textMessage])
			print('An error report has been sent')
		except:
			print('Sending an error report has failed')
		try:
			if booleanText:
				subprocess.call(['python',rootDirectory + 'Emailer.py',textSender,addressFormat,'Alert:',textMessage])
		except:
			print('There was an error sending Text alert!')
except:
	print('Backup was not required')
