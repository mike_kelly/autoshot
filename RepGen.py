import os
import sys
import datetime
import time
import subprocess
from xml.dom import minidom


def stripTag (xmlElement):
	#xmlElement = xmlElement.toxml()
	position = xmlElement.find('>')
	tagName = xmlElement[1:position]
	openingTag = '<' + tagName + '>'
	closingTag = '</' + tagName + '>'
	tagData = xmlElement.replace(openingTag,'').replace(closingTag,'')
	return tagData



try:
	file = sys.argv[1]  # Gets log file input from command line argument
except:
	exit(print('Argument required for log file processing'))	# Reports error and exits program


if os.path.exists(file):			# data available in the file name e.g.(computer name, date info, backup type)
	log = open(file, 'r')			# Opens file for reading
	log_readout = log.readlines()  	# log_rows is a list of every line in the file assigned to log
	log.close()
	row_count = len(log_readout)	# Used to parse for session duration
	fileFound = 1
else:
	fileFound = 0
	sys.exit(print('Input File Not Found'))

#############################################################################################################################################################################################
def job_parse(log_rows):

	jobStart_lines = []								# The list that will hold the line numbers for all starting lines for each job in the log file
	lineCount = len(log_rows)						# Counts the number of lines in the file so the last line can be assigned 
	lineID = 0										# Sets the initial value of lineID, this will be appended to jobStart_files when a starting line is found then incremented

	for each_line in log_rows:						# Searches each line for indications of the start of a backup job and adds that line number to jobStart_lines
		startline_test = each_line.find('Disks')	# checks fot the start of each backup job in the log file 'log_rows'
		if startline_test >= 0:						# If there is a positive result in startline_test then the start of a job has been found
			jobStart_lines.append(lineID)			# appends the line number of any positive hits to the jobStart_lines list
		lineID = lineID + 1							# Keeps track by counting 1 each cycle of what line the loop is on

	jobCount = len(jobStart_lines)					# counts how many jobs exist
	jobStart = []   								# List for the job starting information
	jobEnd = []										# List for the job Ending information

	count = 1
	for each in range (0,jobCount):
		jobStart.append(jobStart_lines[each])
		if each == jobCount and count == 0:
			jobEnd.append(lineCount)
		elif count == 0:
			jobEnd.append(jobStart_lines[each] - 1)
		count = 0
	jobEnd.append(lineCount)


	jobPass = []									# This list is populated with pass / nopass values as each job is tested e.g. (0) = fail (1) = pass
	for each_job in range (0,jobCount):  			# searches each job using jobCount+1 as the cap for the range
		jobPass.append(0)							# the loop populates the jobPass list with as many 0's as there are jobs


	for each_job in range (jobCount):											# Loops through each job
		for eachline in range(jobStart[each_job],jobEnd[each_job]):				# and runs this for loop running on a test of each line of each
			test = log_rows[eachline].find('Snapshot finished successfully')	# job based on 'each_job' of the first loop to select the jobStart and jobEnd items
			if test >= 0:
				jobPass[each_job] += 1

	jobInfo = []
	for each in range (jobCount):
		job = [jobPass[each],jobStart[each],jobEnd[each]]
		jobInfo.append(job)
	#print(jobInfo)

	return(jobInfo)
	# job_parse output is formatted as such
	# jobCount is an int storing the total number of jobs recorded in the file being processed
	# jobPass is a list that stores the pass/fail record for each recorded job starting from the left with the first job
	# jobStart is a dictionary with the starting lines for each job starting with 1
	# jobEnd is also a dictionary, it stores the ending lines for each job starting with 1
#############################################################################################################################################################################################

#############################################################################################################################################################################################
def time_parse(start_line,end_line,log_rows): 					# Parses log file for the start time and end time of the complete job


#### Starting Time ####	
	for each_line in range (start_line,end_line):				# This loop runs through each item in the log_rows list and searches it for a ':' and the first one it comes across should be the starting time
		startline_test = log_rows[each_line].find(':') 
		if startline_test >= 0 and startline_test <= 4:			# The if statement checks the value of startline_test to verify the colon was found close enought to the begining of the line
			start_time = log_rows[each_line][0:8] 				# This strips out the 8 digit time code from the line and saves it as start_time
			break


#### Ending Time ####

	for each_line in reversed(range(start_line,end_line)):		# This loop takes log_rows and reads through it backwards using the row_count_reverse list to select what line to check
		#print(each_line)
		endline_test = log_rows[each_line].find(':')			# This searches each line for a colon and the first one it comes across should be the end time for the log file		
		if endline_test >= 0 and endline_test <= 4:
			end_time = log_rows[each_line][0:8]
			break
	return(start_time,end_time)
#############################################################################################################################################################################################

#############################################################################################################################################################################################
def delta(T1,T2):   # Finds the difference between two times given as strings e.g. '00:00:00'. T1 should be the older of the two values

	(T1hours,T1mins,T1seconds) = T1.split(':')  # Splits the values on the string time stamp into HH MM SS variables
	(T2hours,T2mins,T2seconds) = T2.split(':')

	T1hours = int(T1hours)		# Converts the string time stamp values into integers to create a datetime object with
	T1mins = int(T1mins)
	T1seconds = int(T1seconds)

	T2hours = int(T2hours)
	T2mins = int(T2mins)
	T2seconds = int(T2seconds)

	T1 = datetime.datetime(1900,1,1,T1hours,T1mins,T1seconds) # datetime object created :)
	T2 = datetime.datetime(1900,1,1,T2hours,T2mins,T2seconds)

	delta = T2 - T1     # creates a datetime delta object e.g. '000 days, 00:00:00'
	delta = str(delta)  # Converts delta object to a string to output
	return(delta[-8:])  # Quick hacky output of only 00:00:00 (doesn't do days)  Needs a test to see if duration is longer than a day
						# For that to work I need to parse the full date from the file name, not just the time possibly a function that
						# parses the file name and creates a datetime object ??? maybe
#############################################################################################################################################################################################

#############################################################################################################################################################################################	
def name_parse(start_line,end_line,log_rows): 				# Parses log file for the first jobs backup file name e.g *.sna
															# I would like to expand this function to include returns for all

	for each_line in range (start_line, end_line):			# This loop runs through each item in the range given in log_rows list and searches it for a ':' and the first one it comes across should be the starting time
		startline_test = log_rows[each_line].find('.sna')  	# startline_test holds the value of the results and if it is positive then it has found the file extention
		if startline_test >= 0:								# The if statement checks the value of startline_test to verify the colon was found close enought to the begining of the line
			line_number = log_rows[each_line]							# If startline_test is not negative thent he test was positive and the file extension was found and the line number is saved to line_number
			end_position = startline_test + 4				# Add 4 to the startline test so that end_position will equal the position after .sna and not the position of the period			
			break


	fileName_start = line_number.find('>')					# searches line_number (which contains the .sna file name) for '>' This only appears once and is two characters before the File location and file name
	file_LocationName = line_number[fileName_start + 2:]	# Saves the file location and name to file_LocationName variable (adds two to filename_start to cut off the '>[space]' that starts the string


	fileChar_count = len(file_LocationName)					# character count in the file_LocationName string (file_LocationName contains the file location and file name)

	searchFile_char = '>'  									# backslash is saved to a string so that it can be searched for and not used to escape the following character 

	for each_char in range(1,fileChar_count):				# This loop extracts the file name from
		char_test = line_number[each_char].find(searchFile_char)
		if char_test == 0:
			startFile_position = each_char + 2      		# Adding two to each_char corrects startFile_position to be two posistions right of the '>' character which is the begining of the file location
			break


	file_name = line_number[startFile_position:end_position]	#
	name_frmtd = file_name.split('_')							#

	length = len(name_frmtd[0])							# split name_frmtd[0] at the backslashes and rejoin all except for the last one which is the computer name
														# dont' forget to count how many you are rejoining, if that actually matters pretty sure it will

	location_split = name_frmtd[0].split('\\')			# Splits the name_frmtd[0] into pieces ( drive\location\computer name )
	nameposition = len(location_split) - 1				# gets the length of the location_split list which is the position in the list where the computer name resides 
	location_semifinal = []								# location_semifinal will house the stripped out location information to be joined up later

	for int in range (0, nameposition):					# populates location_semifinal[] with the items  in location_split to be joined (without the computer name)
		location_semifinal.append(location_split[int])

	location_final = '\\'.join(location_semifinal)
	name_type = name_frmtd[3].split('.')				# Removes the .sna for the type string e.g diff.sna to diff
	location_final += '\\'								# Adds a backslash to the end of the location string for formatting purposes

	file_name = '_'.join([location_split[nameposition],name_frmtd[1],name_frmtd[2],name_type[0]]) 	# recompiling the file name from the pieces collected throughout the program
	file_name += '.sna'																				# I believe this piece of info already exists in an earlier variable - fix later

	date = name_frmtd[2].split('-')

	if date[0] == 'Jan':		#
		date[0] = '01'			##
	elif date[0] == 'Feb':		###
		date[0] = '02'			####
	elif date[0] == 'Mar':		#####
		date[0] = '03'			######
	elif date[0] == 'Apr':		#######
		date[0] = '04'			########
	elif date[0] == 'May':		#########
		date[0] = '05'			##########
	elif date[0] == 'Jun':		###########
		date[0] = '06'			############ Checks the months string and converts it to a number (01 - 12) and stores it as the same list entry
	elif date[0] == 'Jul':		############
		date[0] = '07'			###########
	elif date[0] == 'Aug':		##########
		date[0] = '08'			#########
	elif date[0] == 'Sep':		########
		date[0] = '09'			#######
	elif date[0] == 'Oct':		######
		date[0] = '10'			#####
	elif date[0] == 'Nov':		####
		date[0] = '11'			###
	elif date[0] == 'Dec':		##
		date[0] = '12'			#


	date_frmtd = '-'.join(date)		# recompiles date from the date list
	
	
	
	file_count = 0
	filenameComponents = file_name.split('.')
	fileExists = 1

	while fileExists == 1:										# This takes the file location and file name and searches for each file incrementing the file extension
		if file_count == 0:										# one by one .sna .sn1 .sn2... .s10 .s11  It checks if file exists and adds to the file_count variable
			file = location_final+filenameComponents[0]+'.sna'	# the first file is always .sna so if file_count is == 0 then we know we are looking file.sna

			if os.path.exists(file):
				file_count = file_count + 1
			else:
				fileExists = 0

		elif file_count > 0 and file_count < 10:			# for files .sn1 - sn9
			fileString = str(file_count)
			file = location_final+filenameComponents[0]+'.sn'+fileString

			if os.path.exists(file):
				file_count = file_count + 1
			else:
				fileExists = 0

		elif file_count > 9 and file_count < 100:			# for files .s10 - .s99
			fileString = str(file_count)
			file = location_final+filenameComponents[0]+'.s'+fileString

			if os.path.exists(file):
				file_count = file_count + 1
			else:
				fileExists = 0

		elif file_count > 99 and file_count < 1000:			# for files .100 - .999
			fileString = str(file_count)
			file = location_final+filenameComponents[0]+'.'+fileString

			if os.path.exists(file):
				file_count = file_count + 1
			else:
				fileExists = 0

	return(location_final,location_split[nameposition],name_frmtd[1],date_frmtd,name_type[0],file_count,file_name)
############################################################################################################################################################################

def convert_bytes(bytes):
    bytes = float(bytes)
    if bytes >= 1099511627776:
        terabytes = bytes / 1099511627776
        size = '%.2fT' % terabytes
    elif bytes >= 1073741824:
        gigabytes = bytes / 1073741824
        size = '%.2fG' % gigabytes
    elif bytes >= 1048576:
        megabytes = bytes / 1048576
        size = '%.2fM' % megabytes
    elif bytes >= 1024:
        kilobytes = bytes / 1024
        size = '%.2fK' % kilobytes
    else:
        size = '%.2fb' % bytes
    return size
##############################################################################################################################################################################


jobInfo = job_parse(log_readout) # This parses the file for job count info and other needed information to feed into the other functions
session = []
job =[]
jobCount = len(jobInfo) # counts how many job groups that job_parse returns (saved in jobInfo) Includes pass/fail bit, startline of job, endline of job
jobSuccess = 0

for each in range(jobCount):
	if jobInfo[each][0] == 1:
		jobSuccess += 1


ticker = 1 # a bit for flow control stopping the if statement from running multiple times
for each in range(0,jobCount):
	nameInfo = name_parse(jobInfo[each][1],jobInfo[each][2],log_readout)  	# This sends the start and endline from jobInfo(job_parse function) to name_parse and returns the value as nameInfo
	times = time_parse(jobInfo[each][1],jobInfo[each][2],log_readout)		# Same as above feeds jobInfo into time_parse and outputs the return to times
	duration = delta(times[0],times[1])										# takes the times puts it into the delta function and saves the delta output to duration (this is job duration, not session duration

	if ticker == 1:                								# The if statement runs once and gathers the info for the session header list
		session.append(nameInfo[3])								# Date
		session.append(nameInfo[0])								# Save Location
		session.append(jobCount)								# '# of jobs'
		sessionTime = time_parse(0,row_count,log_readout)		# Session Duration
		sessDuration = delta(sessionTime[0],sessionTime[1])		# Session Duration
		session.append(sessDuration)							# session Duration
		session.append(sys.path[0])								# logfile location

		session.append(file)									# logfile Name
		ticker += 1												# this adds to the ticker which prevents the if statement from running multiple times
	job.append([nameInfo[1],nameInfo[2],jobInfo[each][0],nameInfo[4],duration,nameInfo[6],nameInfo[5]]) 	# This agregates the job info for each job and adds it to the job list
		# Archive size still needs to be added to this list
		# Once that is added all data can be output for emailing :D Woot!!

int(jobCount)

if jobCount > 1:
	pluralCorrection = 'sessions'
else:
	pluralCorrection = 'session'

slashPosition = 0									# This section strips the directory from the log file input
stringLen = len(session[5])
gate = 1
while gate == 1:									# finds the backslash working backwards through the sting
	charpos = session[5][stringLen-1].find('\\')	# once the backslash is found the stringLen is the position
	if charpos == 0:
		logFile = session[5][stringLen:]
		logDir = session[5][0:stringLen]
		gate = 0
	else:
		stringLen = stringLen - 1
	
## This section is the header section of the email ##################################################################################
body = """
Archive Date:				%s
Archive Storage Location:	  %s
Number of Sessions:		   %d
Backup Duration:		     %s
Log File Storage Location:	  %s
Attached Log File Name:		 %s

   %s of %d %s were successfully completed without errors
""" %(session[0],session[1],session[2],session[3],logDir,logFile,jobSuccess,jobCount,pluralCorrection)

######################################################################################################################################

for each in range (jobCount):
	if job[each][2] == 1:						# fail / pass test for each job
		successTest = '-- Completed Successfully'
	else:
		successTest = '-- Backup Failed'


	archiveTotal = 0
	for all in range (0,job[each][6]):    	# Constructs the extension name of the archive files for collecting file size info
		fileName = job[each][5].split('.') 	# splits the file name  from the file extension
		if all == 0:						# This builds a new file extension to the rules that snapshot uses .sna, .sn1, .sn9, .s10, .s99, .100 (tested up to .100, probly won't ever need over 999 but don't have the resources to try)
			ext = 'sna'
			ext = str(ext)
		elif all > 0 and all <10:
			ext = 'sn' + str(all) 			#converts to string so they can be added together
		elif all >= 10 and all < 100:
			ext = 's' + str(all)
		elif all > 99:
			ext = str(all)

		fileName = session[1] + fileName[0] +'.' + ext # adds directory and file name together and passes it on to os.path.getsize

		filesize = os.path.getsize(fileName)   # goes and grabs filesize

		archiveTotal += filesize # adds filesize to the total


	archiveTotal = convert_bytes(archiveTotal) # uses the convert_bytes function to convert to the easiest readable size set (KB, MB, GB, TB)
	archiveTotal += 'B'

### This section loops producing each job section ###########################################################################
	body = body + """
	%s  %s - Drive  %s
	-------------------------------------------------------------------
	Archive Type:			  	  %s
	Session Duration:		        %s
	Archive File Name:		       %s
	Archive File Count:			%d
	Archive Total Size:			%s
	"""	%(job[each][0],job[each][1],successTest,job[each][3],job[each][4],job[each][5],job[each][6],archiveTotal)
#############################################################################################################################


# configuration XML file must be in the same directory
xmldoc = minidom.parse('config.xml')

# configNode is the main node
configNode = xmldoc.firstChild

# Email variable xml parsing
emailNode = configNode.childNodes[3]
recipiantsNode = emailNode.childNodes[7]
fromAddress = stripTag(emailNode.childNodes[3].toxml())
firstRecipiant = stripTag(recipiantsNode.childNodes[3].toxml())

subject = 'Automated Backup Report'

try:
	subprocess.call(['python','Emailer.py',fromAddress,firstRecipiant,subject,body])
	print('Report sent successfully')
except:
	print('There was an error sending the report email')